#include "picross.h"

using namespace std;

int main(int argc, char** argv){
	PS::Picross puzzle(argv[1]);
	puzzle.solve();
	
	return 0;
}
