#include "picross.h"

#include <iostream>

namespace PS{

	class TestPicross{
		public:
			TestPicross(){}
			~TestPicross(){}
		public:
			void checkIntraStripe(const PS::Hint& hint, PS::Stripe& stripe);
	};

	void TestPicross::checkIntraStripe(const PS::Hint& hint, PS::Stripe& stripe){
		PS::Picross picross("milk.dat");
		for (size_t i = 0; i < hint.size(); i++){
			std::cout << hint[i] << " ";
		}
		std::cout << std::endl;
		picross.printStripe(stripe);
		picross.checkIntraStripe(hint, stripe);
		picross.printStripe(stripe);
	}

}

using namespace std;

int main(int argc, char** argv){
	{
		// 3
		PS::Hint hint(1, 3);
		PS::Stripe stripe(5, PS::UNKNOWN);
		stripe.push_back(PS::BLANK);
		PS::TestPicross test;
		test.checkIntraStripe(hint, stripe);
	}
	{
		// 1 1
		PS::Hint hint(2, 1);
		PS::Stripe stripe(5, PS::UNKNOWN);
		stripe.push_back(PS::BLANK);
		PS::TestPicross test;
		test.checkIntraStripe(hint, stripe);
	}
	{
		// 1 3
		PS::Hint hint(1, 1);
		hint.push_back(3);
		PS::Stripe stripe(5, PS::UNKNOWN);
		stripe.push_back(PS::BLANK);
		PS::TestPicross test;
		test.checkIntraStripe(hint, stripe);
	}
	{
		// 
		PS::Hint hint(0);
		PS::Stripe stripe(5, PS::UNKNOWN);
		stripe.push_back(PS::BLANK);
		PS::TestPicross test;
		test.checkIntraStripe(hint, stripe);
	}
	{
		// 5
		PS::Hint hint(1, 5);
		PS::Stripe stripe(5, PS::UNKNOWN);
		stripe.push_back(PS::BLANK);
		PS::TestPicross test;
		test.checkIntraStripe(hint, stripe);
	}
	{
		// 2 2
		PS::Hint hint(2, 2);
		PS::Stripe stripe(5, PS::UNKNOWN);
		stripe.push_back(PS::BLANK);
		PS::TestPicross test;
		test.checkIntraStripe(hint, stripe);
	}
	{
		// 4
		PS::Hint hint(1, 4);
		PS::Stripe stripe(5, PS::UNKNOWN);
		stripe.push_back(PS::BLANK);
		PS::TestPicross test;
		test.checkIntraStripe(hint, stripe);
	}
	{
		// 1 1
		PS::Hint hint(2, 1);
		PS::Stripe stripe;
		stripe.push_back(PS::SOLID);
		stripe.push_back(PS::BLANK);
		stripe.push_back(PS::UNKNOWN);
		stripe.push_back(PS::BLANK);
		stripe.push_back(PS::SOLID);
		stripe.push_back(PS::BLANK);
		PS::TestPicross test;
		test.checkIntraStripe(hint, stripe);
	}
	return 0;
}
