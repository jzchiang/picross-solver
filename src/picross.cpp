#include "picross.h"

#include <cstdio>
#include <cstring>
#include <iostream>

#define LINE_MAX 64

namespace PS{
	Picross::Picross(const char* dat_file_name){
		this->parseDat(dat_file_name);
		return;
	}

	void Picross::solve(){
		this->initAllStripes();
		while(!this->checkSolved()){
		// for (int i = 0; i < 5; i++){
			for (size_t i = 0; i < this->nrows; ++i){
				this->checkIntraStripe(this->hint_rows[i], this->rows[i]);
			}
			for (size_t j = 0; j < this->ncols; ++j){
				this->checkIntraStripe(this->hint_cols[j], this->cols[j]);
			}
			
			for (size_t i = 0; i < this->nrows; ++i){
				this->checkInterStripe(this->cols, i, this->rows[i]);
			}
			for (size_t j = 0; j < this->ncols; ++j){
				this->checkInterStripe(this->rows, j, this->cols[j]);
			}
			this->printSolution();
		}
		return;
	}

	void Picross::parseDat(const char* dat_file_name){
		this->hint_rows.clear();
		this->hint_cols.clear();
		FILE* dat_file_handle= fopen(dat_file_name, "r");
		char buf[LINE_MAX];
		if (fgets(buf, LINE_MAX, dat_file_handle) == NULL){
			// Line read error
		}
		sscanf(buf, "%u %u", &this->nrows, &this->ncols);

		// Blank line
		if (fgets(buf, LINE_MAX, dat_file_handle) == NULL){
			// Line read error
		}

		for (int i = 0; i < this->nrows; ++i){
			if (fgets(buf, LINE_MAX, dat_file_handle) == NULL){
				// Line read error
			}
			Hint row;
			char* pch = strtok (buf," ");
			while (pch != NULL) {
				unsigned int block;
				sscanf(pch, "%u", &block);
				row.push_back(block);
				pch = strtok (NULL, " ");
			}
			this->hint_rows.push_back(row);
		}
		this->nrows++;
		this->hint_rows.push_back(Hint(0));

		// Blank line
		if (fgets(buf, LINE_MAX, dat_file_handle) == NULL){
			// Line read error
		}

		for (int i = 0; i < this->ncols; ++i){
			if (fgets(buf, LINE_MAX, dat_file_handle) == NULL){
				// Line read error
			}
			Hint col;
			char* pch = strtok (buf," ");
			while (pch != NULL) {
				unsigned int block;
				sscanf(pch, "%u", &block);
				col.push_back(block);
				pch = strtok (NULL, " ");
			}
			this->hint_cols.push_back(col);
		}
		this->ncols++;
		this->hint_cols.push_back(Hint(0));

		fclose(dat_file_handle);
		return;
	}

	void Picross::initAllStripes(){
		this->rows = std::vector< Stripe >(nrows, this->initStripe(ncols));
		this->cols = std::vector< Stripe >(ncols, this->initStripe(nrows));
	}

	Stripe Picross::initStripe(const unsigned int n){
		Stripe stripe(n, UNKNOWN);
		return stripe;
	}

	bool Picross::allBlankOrUnknown(const Stripe& stripe){
		for(Stripe::const_iterator px = stripe.begin(); px != stripe.end(); px++){
			if (*px == SOLID){
				return false;
			}
		}
		return true;
	}

	// TODO Rename to catalogMultiverse
	Multiverse Picross::genStripe(const Hint& hint, const Stripe& stripe){
		Multiverse retval;
		size_t nstripe = stripe.size();
		if (hint.size() == 0){
			if (this->allBlankOrUnknown(stripe)){
				retval.insert(Stripe(nstripe, BLANK));
			}
		}else{
			// i_block_start is start of block
			// i_block_start+hint[0] is end of block
			// i_block_start+hint[0]+1 is separator
			for (size_t i_block_start = 0; i_block_start + hint[0] < nstripe; i_block_start++){
				// Remove element 0
				Hint minihint(hint.begin()+1, hint.end());
				// Get substripe from element i_block_start+hint[0]+1 to end
				Stripe ministripe(stripe.begin() + i_block_start+hint[0]+1, stripe.end());
				// Get all eigenstates with block in this position
				Multiverse getval = this->genStripe(minihint, ministripe);
				for (Multiverse::const_iterator pgv = getval.begin(); pgv != getval.end(); pgv++){
					// Block associated with this hint
					Stripe my_block(0);
					// Add leading blanks
					Stripe blank_block(i_block_start, BLANK);
					my_block.insert(my_block.end(), blank_block.begin(), blank_block.end());
					// Create a solid block of length hint[0]
					Stripe solid_block(hint[0], SOLID);
					my_block.insert(my_block.end(), solid_block.begin(), solid_block.end());
					// Add separator after
					my_block.push_back(BLANK);
					// Add subeigenstates
					my_block.insert(my_block.end(), pgv->begin(), pgv->end());
					// Append to overall eigenstates
					bool valid_sequence = true;
					for (size_t i = 0; i < nstripe; i++){
						if ((stripe[i] == BLANK && my_block[i] == SOLID) ||
								(stripe[i] == SOLID && my_block[i] == BLANK)){
							valid_sequence = false;
						}
					}
					if (my_block.size() != stripe.size()){
						valid_sequence = false;
						std::cout << "unexpected size" << std::endl;
						this->printStripe(stripe);
						this->printStripe(my_block);
					}
					if (valid_sequence){
						retval.insert(my_block);
					}
				}
			}
		}
		if (retval.size() == 0){
		}
		return retval;
	}

	Stripe Picross::collapseStripe(const Multiverse& multiverse, const size_t n){
		// Find first valid universe
		Multiverse::const_iterator pmv = multiverse.begin();
		Stripe stripe;
		for ( ; stripe.size() == 0; pmv++){
			if (pmv->size() == n){
				stripe = *pmv;
			}else{
				this->printStripe(*pmv);
			}
		}
		// Iterate through all universes
		for ( ; pmv != multiverse.end(); pmv++){
			if (pmv->size() == n){
				for (size_t i = 0; i < stripe.size(); i++){
					// If alternate universe conflicts with main universe, set cell to UNKNOWN
					if (stripe[i] != (*pmv)[i]){
						stripe[i] = UNKNOWN;
					}
				}
			}
		}
		return stripe;
	}

	void Picross::checkIntraStripe(const Hint& hint, Stripe& stripe){
		// Based on hint and current stripe, generate all possible stripes
		Multiverse multiverse = this->genStripe(hint, stripe);
		if (multiverse.size() == 0){
			std::cout << "impossible multiverse" << std::endl;
			for (size_t i = 0; i < hint.size(); i++){
				std::cout << hint[i] << " ";
			}
			std::cout << std::endl;
			this->printStripe(stripe);
		}
		for (Multiverse::const_iterator pmv = multiverse.begin(); pmv != multiverse.end(); pmv++){
			// this->printStripe(*pmv);
		}
		// Scan through all possible stripes. If all CellStates for a cell agree, lock it in. Otherwise, set to UNKNOWN
		stripe = this->collapseStripe(multiverse, stripe.size());
	}

	void Picross::checkInterStripe(const std::vector< Stripe >& cross_stripes, const size_t k, Stripe& stripe){
		for (size_t i = 0; i < stripe.size(); ++i){
			if (stripe[i] == UNKNOWN){
				stripe[i] = cross_stripes[i][k];
			}
		}
	}

	bool Picross::checkSolved(){
		for (size_t i = 0; i < this->nrows; ++i){
			for (size_t j = 0; j < this->ncols; ++j){
				if (this->rows[i][j] == UNKNOWN){
					return false;
				}
				if (this->cols[j][i] == UNKNOWN){
					return false;
				}
			}
		}
		return true;
	}

	void Picross::printHint(const Hint& hint){
		for (size_t i = 0; i < hint.size(); i++){
			std::cout << hint[i] << " ";
		}
		std::cout << std::endl;
	}

	void Picross::printStripe(const Stripe& stripe){
		for (size_t i = 0; i+1 < stripe.size(); i++){
			if (stripe[i] == SOLID){
				std::cout << "\u2588";
			}else if(stripe[i] == BLANK){
				std::cout << "X";
			}else if(stripe[i] == UNKNOWN){
				std::cout << " ";
			}
		}
		std::cout << std::endl;
		return;
	}

	void Picross::printSolution(){
		for (size_t i = 0; i+1 < this->rows.size(); i++){
			Stripe stripe = this->rows[i];
			this->printStripe(stripe);
		}
		std::cout << std::endl;
		return;
	}
}
