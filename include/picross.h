#include <set>
#include <vector>

#ifndef __PICROSS_H__
#define __PICROSS_H__

namespace PS{
	typedef std::vector<unsigned int> Hint;

	enum CellState {SOLID, BLANK, UNKNOWN};

	typedef std::vector<CellState> Stripe;

	typedef std::set<Stripe> Multiverse;

  class Picross{
  public:
    Picross(const char* dat_file_name);
		~Picross(){}
		void solve();
	private:
		/**
			Read and parse data from file. Store in PS::Picross object
		 */
		void parseDat(const char* dat_file_name);

		/**
			Initialize all stripes to UNKNOWN CellState based on nrows and ncols
		 */
		void initAllStripes();

		/**
			Initialize one stripe to UNKNOWN CellState based on n
		 */
		Stripe initStripe(const unsigned int n);

		bool allBlankOrUnknown(const Stripe& stripe);
		Multiverse genStripe(const Hint& hint, const Stripe& stripe);
		Stripe collapseStripe(const Multiverse& multiverse, const size_t n);
		void checkIntraStripe(const Hint& hint, Stripe& stripe);

		/**
			Check Stripe against intersecting Stripes.
			UNKNOWN CellStates are updated if corresponding CellState in intersecting Stripe is SOLID or BLANK
		 */
		void checkInterStripe(const std::vector< Stripe >& cross_stripes, const size_t k, Stripe& stripe);
		bool checkSolved();

		/**
			Print hint
		 */
		void printHint(const Hint& hint);

		/**
			Print stripe
		 */
		void printStripe(const Stripe& stripe);

		/**
			Print solution
		 */
		void printSolution();
		// std::vector< Stripe > generateStripes(const Hint, const unsigned int n);
  private:
		unsigned int nrows;
		unsigned int ncols;
    std::vector< Hint > hint_rows;
    std::vector< Hint > hint_cols;
		std::vector< Stripe > rows;
		std::vector< Stripe > cols;
#ifdef DEBUG
		friend class TestPicross;
#endif
  };
}

#endif
