PICROSS_ROOT          = $(PWD)

-include .make.config
-include $(PICROSS_ROOT)/.make.color

SHELL		      = bash
CC                    = g++
# C_COMPILER_OPTIONS   += -c -ggdb -O3 -x c
# C_LINKER_OPTIONS     += -g
CPP_COMPILER_OPTIONS += -c -ggdb -O3 -Werror -DDEBUG
CPP_LINKER_OPTIONS   += -ggdb

# External Libraries

LOC_LIBS    = 
LIB_EXT += ${LOC_LIBS}

LIB_EXT += 

# Locate Source Code

EXCLUDE               =  # put names of files to ignore
SRC_MAIN              = $(filter-out $(EXCLUDE), $(shell find $(PICROSS_ROOT)/src/ -name '*.cc'))
EXE 		      = $(subst $(PICROSS_ROOT)/src/,$(PICROSS_ROOT)/test/, $(SRC_MAIN:.cc=))
SRC		      = $(filter-out $(EXCLUDE), $(shell find $(PICROSS_ROOT)/src/ -name '*.cpp' ))
HDR		      = $(filter-out $(EXCLUDE), $(shell find $(PICROSS_ROOT)/include/ -name '*.h')) $(filter-out $(EXCLUDE), $(shell find $(PICROSS_ROOT)/src/ -name '*.h'))
LIB 		      = $(subst $(PICROSS_ROOT)/src/,$(PICROSS_ROOT)/lib/, $(SRC:.cpp=.cpp.o))
LIB_MAIN              = $(subst $(PICROSS_ROOT)/src/,$(PICROSS_ROOT)/lib/, $(SRC_MAIN:.cc=.cc.o))
INC 		      = -I$(PICROSS_ROOT)/src \
		        -I$(PICROSS_ROOT)/include \
		        $(INC_EXT)
INP                   = $(filter-out $(EXCLUDE), $(shell find $(PICROSS_ROOT)/src/ -name '*.dat'))
INP_LOC               = $(subst $(PICROSS_ROOT)/src/,$(PICROSS_ROOT)/test/, $(INP))

.SECONDARY: $(LIB) $(LIB_MAIN)

all: make_directories $(EXE) $(INP_LOC)
	@echo -e $(B_ON)$(FG_GREEN)"###"
	@echo -e "### DONE" 
	@echo -e "###"$(RESET)	

make_directories: $(SRC)
	@mkdir -p $(dir $(LIB)) $(dir $(LIB_MAIN)) $(dir $(EXE))

$(PICROSS_ROOT)/bin/%: $(PICROSS_ROOT)/lib/%.o $(LIB) $(HDR)
	@echo -e $(B_ON)$(FG_BLUE)"###"
	@echo -e "### LINKING $@" 
	@echo -e "###"$(RESET)
	$(CC) ${CPP_LINKER_OPTIONS} -o $@ $^ $(LIB_EXT)

$(PICROSS_ROOT)/lib/%.cpp.o: $(PICROSS_ROOT)/src/%.cpp $(HDR)
	@echo -e $(B_ON)$(FG_YELLOW)"###"
	@echo -e "### COMPILING $<" 
	@echo -e "###"$(RESET)
	$(CC) $(CPP_COMPILER_OPTIONS) $(INC) -o $@ $<

$(PICROSS_ROOT)/lib/%.cc.o: $(PICROSS_ROOT)/src/%.cc $(HDR)
	@echo -e $(B_ON)$(FG_YELLOW)"###"
	@echo -e "### COMPILING $<" 
	@echo -e "###"$(RESET)
	$(CC) $(CPP_COMPILER_OPTIONS) $(INC) -o $@ $<

$(PICROSS_ROOT)/test/%: $(PICROSS_ROOT)/lib/%.cc.o $(LIB) $(SRC) $(HDR) make_directories
	@echo -e $(B_ON)$(FG_YELLOW)"###"
	@echo -e "### LINKING $<" 
	@echo -e "###"$(RESET)
	$(CC) $(CPP_LINKER_OPTIONS) $(INC) -o $@ $< $(LIB) $(LIB_EXT)

$(PICROSS_ROOT)/test/%.dat: $(PICROSS_ROOT)/src/%.dat
	@echo -e $(B_ON)$(FG_YELLOW)"###"
	@echo -e "### COPYING $<" 
	@echo -e "###"$(RESET)
	cp $< $@

%.cpp:
	@echo -e $*

%.cc:
	@echo -e $*

%.h:
	@echo -e $*

%.dat:
	@echo -e $*

#
# Utility Targets
#

clean:
	@echo -e $(B_ON)$(FG_RED)"###"
	@echo -e "### Cleaning out ./lib, ./bin, *~, *#" 
	@echo -e "###"$(RESET)	
	rm -rf ${LIB} ${LIB_MAIN} ${EXE} *~ *\#
tidy:
	rm -rf *~ *\# src/*~ src/*\# dat/*~ dat/*\# include/*~ include/*\# 
